var beautify_html = require('js-beautify').html,
    fs = require('fs'),
    Transform = require('stream').Transform,
    tags = require('./tags'),
    utils = require('./utils');

var inFile = process.argv[2];
preProcess(inFile);

function preProcess(filename){

    preprocess_out_file = filename + '-pre'


    var regex_sopen = new RegExp('<script>');
    var regex_sclose = new RegExp('</script>');
    var sopen = false;
    var instream = fs.createReadStream(filename);
    var outstream = fs.createWriteStream(preprocess_out_file)

    var buffer = '';
    var parser = new Transform();

    parser._transform = function(data, encoding, done) {
        var new_buffer = buffer + data;
        var lines = new_buffer.split("\n");

        if (lines.length > 1){
            var last = lines.pop();
            if (utils.endsWith(last, "\n")){
                lines.push(last);
            } else {
                buffer = last;
            }
        }

        for (var i = 0; i < lines.length; i++){
            if (lines[i] === undefined){
                continue;
            }
            if(!sopen){
                sopen = regex_sopen.test(lines[i]);
            } else if (regex_sclose.test(lines[i])){
                sopen = false;
            }
            if(!sopen){
                this.push(preProcessLine(lines[i]))
            } else {
                this.push(preProcessScriptLine(lines[i]));
            }
        }
        done();
    }

    parser._flush = function(done){
        this.push(preProcessLine(buffer));
        done();
    }
    instream.pipe(parser).pipe(outstream);
    outstream.on("finish", function(){
        beautify(filename);
    })

}

function preProcessLine(line){
    var regex = /(".*?\{\%\s*.*?\s*\%\}.*?"|\{\%\s*.*?\s*\%\})/g; // /\{\%\s*.*?\s*\%\}/g
    var results = line.match(regex)
    if (results !== null && results.length >= 0) {
        for (var i = 0; i < results.length; i++){
            var tag = results[i];
            var out = '';
            if(tag[0] != "'" && tag[0] != '"'){
                if (tags.isSingleLineTag(tag)){
                    out = '<templatetag code="' + escape(tag) + '"/>';
                } else if (tags.isMiddleBlockTag(tag)){
                    out = '</templatetag code="' + escape(tag) + '"><templatetag code="">';
                } else {
                    var blockType = tags.isBlockTag(tag);
                    if (blockType === 'open'){
                        out = '<templatetag code="' + escape(tag) + '">';
                    } else if (blockType === 'close'){
                        out = '</templatetag code="' + escape(tag) + '">';
                    }
                }
            line = line.replace(tag, out);
            }
        }
    }
    return line + '\n';
}

function preProcessScriptLine(line){
    var regex = /(".*?\{\%\s*.*?\s*\%\}.*?"|\{\%\s*.*?\s*\%\}|".*?\{\{.*?\}\}.*?"|\{\{.*?\}\})/g; // /\{\%\s*.*?\s*\%\}/g
    var results = line.match(regex)
    if (results !== null && results.length >= 0) {
        for (var i = 0; i < results.length; i++){
            var tag = results[i];
            var out = '';
            if(tag[0] != "'" && tag[0] != '"'){
                if (tags.isSingleLineTag(tag)){
                    out = '/*templatetag code="' + escape(tag) + '"*/';
                } else if (tags.isMiddleBlockTag(tag)){
                    out = '/*templatetag code="' + escape(tag) + '"*//*templatetag code=""*/';
                } else {
                    var blockType = tags.isBlockTag(tag);
                    if (blockType === 'open'){
                        out = '/*templatetag code="' + escape(tag) + '"*/';
                    } else if (blockType === 'close'){
                        out = '/*templatetag code="' + escape(tag) + '"*/';
                    }else{
                        out = '/*templatetag code="' + escape(tag) + '"*/';
                    }
                }
            line = line.replace(tag, out);
            }
        }
    }
    return line + '\n';
}
    
function beautify(baseFilename) {
    var filename = baseFilename + '-pre';
    fs.readFile(filename, 'utf8', function (err, data) {
        if (err) {
            throw err;
        }
        var beautified = beautify_html(data, { indent_size: 4 });

        fs.writeFile(baseFilename + '-beau', beautified, function(){
            postProcess(baseFilename);
        });
    });
}

function postProcess(baseFilename) {
    var postProcessOutFile = 'beautified-' + baseFilename
    var filename = baseFilename + '-beau'
    var instream = fs.createReadStream(filename);
    var outstream = fs.createWriteStream(postProcessOutFile)

    var buffer = '';
    var parser = new Transform();

    var regex_sopen = new RegExp('<script>');
    var regex_sclose = new RegExp('</script>');
    var sopen = false;

    parser._transform = function(data, encoding, done) {
        var lines = (buffer + data).split("\n");

        if (lines.length > 1){
            var last = lines.pop();
            if (utils.endsWith(last, "\n")){
                lines.push(last);
            } else {
                buffer = last;
            }
        }

        for (var i = 0; i < lines.length; i++){
            if (lines[i] === undefined){
                continue;
            }
            if(!sopen){
                sopen = regex_sopen.test(lines[i]);
            } else if (regex_sclose.test(lines[i])){
                sopen = false;
            }
            if(!sopen){
                this.push(postProcessLine(lines[i]))
            } else {
                this.push(postProcessScriptLine(lines[i]));
            }
        }
        done();
    }

    parser._flush = function(done){
        this.push(postProcessLine(buffer));
        done();
    }
    instream.pipe(parser).pipe(outstream);
    outstream.on("finish", function(){
        cleanUp(baseFilename);
    })
}

function postProcessLine(line){
    var regex = /<\/?templatetag code=".*?".*?\/?>/g
    var results = line.match(regex); //utils.getMatches(line, regex, 1)
    if (results !== null && results.length >= 0){ 
        for(var i = 0; i < results.length; i++){
            var ttag = results[i]
            var tag = unescape(ttag.match(/"(.*?)"/)[1]);
            line = line.replace(ttag, tag);
        }
    }
    return line + '\n';
}

function postProcessScriptLine(line){
    var regex = /\/\*templatetag code=".*?"\*\//g
    var results = line.match(regex); //utils.getMatches(line, regex, 1)
    if (results !== null && results.length >= 0){ 
        for(var i = 0; i < results.length; i++){
            var ttag = results[i]
            var tag = unescape(ttag.match(/"(.*?)"/)[1]);
            line = line.replace(ttag, tag);
        }
    }
    return line + '\n';
}


function cleanUp(baseFilename){
    fs.unlinkSync(baseFilename + '-pre');
    fs.unlinkSync(baseFilename + '-beau');
    console.log("Done!");
}
