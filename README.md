HTML-Template-Beautifier
========================

forked from https://github.com/bennettaur/HTML-Template-Beautifier
from Michael Bennett https://github.com/bennettaur

A wrapper node.js app for https://github.com/einars/js-beautify HTML beautifier
that causes django template tags to be formatted like HTML tags.

This makes template files a lot easier to read and clearly identifies what code
is contained between block tags.
